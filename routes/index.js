const express = require('express');
const router = express.Router();
const axios = require('axios');
const mcache = require('memory-cache');

const numverify_api_key = '18ea5723fe6fedd3c91a6919e50f28bf';

const cache = (duration) => {
    return (req, res, next) => {
        let key = '__express__' + req.originalUrl || req.url;
        let cacheBody = mcache.get(key);
        if (cacheBody) {
            res.send(cacheBody);
        } else {
            res.sendResponse = res.send;
            res.send = (body) => {
                mcache.put(key, body, duration * 1000);
                res.sendResponse(body)
            };
            next();
        }
    }
};

const api = axios.create({
    baseURL: 'http://apilayer.net/api/'
});

/* numverify proxy. It cache response 30 minutes (Minutes * 60) */
router.get('/validate', cache(30 * 60),function(req, res, next) {
    return api.get("/validate",{
        params: {
            access_key: req.query.access_key != null ? req.query.access_key : numverify_api_key,
            number: req.query.number,
            format: req.query.format === null ? 1 : req.query.format,
            country_code: req.query.country_code !== null ? req.query.country_code : ''
        }
    }).then( function (response) {
            res.send(response.data);
        }
    ).catch(function (error) {
        console.log(error);
        res.send("Could not validate number");
    });
});

module.exports = router;
